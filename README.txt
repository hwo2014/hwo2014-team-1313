CLASS STRUTCTURE:
    Each neural network should be represented by a class (OSC, OLC, TC, STG)
    They pottentially run in different threads
    Some computationally expensive threads like STG might not be instantly responsive
    Other threads like TC need to respond with the ticks of the game


PROGRAM FLOW:
    PRELIM:
        Create initial
        Analyze shape of track and determine expermint locations
        Carry out experiments and record data
        Analyze data and give it to whoever needs it
        OSC and OLC calculate routes
        STG tries to drive optimal route and make adjustments
        OSC and OLC recalcalculate based on back propagation
    RACE:
        
        


PRELIM TESTS:
    deceleration on straight from known speed
    deceleration in curve
    maximum slip angle before crash
    acceleration from 0 to known speed
    time/speed loss to change lanes?

COMPUTED DATA:
    Speed:
        difference between previous and current inPieceDistance / tick time
    Acceleration:
        difference in speed over tick time

NUERAL NETWORK(S):
    Optimum Speed Calculator (OSC):
        Interleves with OLC
        Determines the optimal speed for each lane/piece
            Maximum possible speed
            Desired speed based on future pieces
        Takes into account following pieces (works course in reverse)
        Learned data affects all lanes in pieces (relatively)
        OUTPUT:
            time it takes to drive lane based on speed

    Optimal Lane Calculator (OLC):
        Interleves with OSC
        Determine the optimal places to change lanes
        Do we want to change lanes on upcoming switch
        Thinks of track in sections (section = pieces between changes)
            Switch is last piece of each section
        Take into account any penalty for switchting
        Calculate difference between lanes for section
        INPUTS:
            top speed for lane for section * length
        DETERMINES:
            If we should change lanes at switch

    Throttle Controller (TC):
        Responsible for controlling the throttle achieving optimal speed
        Takes into account slip angle
        Feeds data back to OSC
        Gets data async from STG and OSC
        Performs actions based on data it has
        Saves current plan for track which is adjusted as instructed by STG/OSC/OLC
        INPUTS:
            Current Speed (we calculate that)
            Slip Angle
            Optimal Speed for lane/piece(determined by OSC)
        DETERMINES:
            what current throttle should be
            changes the lane
            
    Strategizer(STG):
        Uses info from OSC and OLC to pick fastest route to start
        Handles Crashes and 
        ?Dealing with other AI?
        Responsible for learning and adjusting
        Adjustments:
            slip angle vs maximum speed for piece
            crashes
            collisions

CLASSES:
    RBT:
        bot handles passing messages and server communication
    OSC:
        thread that handles speed calculations
    OLC:
        thread that handles lane calculations
    STG:
        thread that handles strategy
    TC:
        class in main thread that hold current plan and control throttle & lane changes
    Section:
        Pieces[]
    Track:
        Pieces[]
    Pieces:
        Lanes[]
    Lane:
        AchievableSpeed
        GuessMaxSpeed
        KnownMaxSpeed
        DesiredSpeed
        Length

    Sections:


THINGS TO FIGURE OUT:
    what to use for neural network?
        how to train
        how to save trainging data
        how to consume trainging data
    
    speed controller algorithm

    multiplayer strategy

TODO:
    TC
    OSC
    OLC
    STG
