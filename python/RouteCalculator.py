# Route Calculator

# responsible for predicting maximum & ideal speeds for individual pieces
# and lane-change costs for the track as whole

import math

class RouteCalculator:
    def __init__(track, laps):
        # is slip a function of angular acceleration or centripetal acceleration?
        self.max_aa = 1.0 # determine
        self.max_ca = 1.0
        self.curve_spd_fn = self.maxCurveSpdCA
        
        self.track = track
        self.sections = self.getTrackSections(track.pieces, track.lanes, laps)

    def maxCurveSpdCA(self, radius):
        return math.sqrt(self.max_ca * radius)

    def maxCurveSpdAA(self, radius):
        return self.max_aa * radius

    def maxPieceLaneSpd(self, piece, lane):
        if length in piece:
            return float('inf')
        
        radius = piece.radius + lane.distanceFromCenter
        return self.curve_spd_fn(self, radius)

    def getTrackSections(pieces, lanes, laps):
        piece_sets = [[]]
        for l in range(laps):
            for i, piece in enumerate(pieces):
                piece_sets[-1].append(i)
                if switch in piece and piece.switch == True:
                    piece_sets.append([])

        sections = []
        for i, pieces in enumerate(piece_sets):
            section = Section(i, pieces, lanes)
            if i > 0:
                sections[i-1].successor = section
            sections.append(section)

        return sections

    # generate guesses about optimal routing through this track
    def getTrackAnalysis():
        for section in reversed(self.sections):
            section.updateGuess(track_data)

    # provide the RC with a datapoint about max spd in a
    # particular section
    def addPieceLaneSpdData(p_index, l_index, spd, angle):
        self.pieces[p_index].addLaneSpdData(l_index, spd, angle)

    def accelCurve():
        pass

class Data:
    def __getattr__(self, name):
        return self.data[name]

class Piece(Data):
    def __init__(index, data, lanes):
        self.index = index
        self.lanes = [Piece.Lane(self, lane) for lane in lanes]
        self.data = data

    # update lane max speed estimates based on data point
    def addLaneSpdData(l_index, spd, angle):
        pass

    class Lane(Data):
        def __init__(piece, data):
            self.piece = piece
            self.data = data

class Section:
    def __init__(index, pieces, lanes):
        self.index = index
        self.pieces = pieces
        self.successor = None
        self.lanes = [Section.Lane(self, lane) for lane in lanes]

    class Lane(Data):
        def __init__(section, data):
            self.section = section
            self.data = data

        # what's the fastest the car could exit this section,
        # given an entry spd and any curves in this section?
        def maxExitSpd(self, entry_spd):
            pass

        # what are the optimal speeds in each piece/lane of this
        # section, given a maximum exit speed?
        def optimalPieceSpds(self, exit_spd):
            pass

        # which switch decision is optimal when exiting?
        def optimalSwitch(self, lanes):
            pass

        # how long will it take to drive this?
        def timeToDrive(self, entry_spd, piece_spds):
            pass
    

