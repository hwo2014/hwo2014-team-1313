#####################################
# ThrottleController
#   This class is responsible for actually driving
#   it sets the throttle and issues lane changes
#   it maintains a "map" of the whole track with desired metrics to achieve
#   it takes in slip angle directly
#   it also takes in data from the OSC, OLC, and STG objects
#
#   TODO:
#       respond to bot with throttle
#       respond to bot with lane change
#       come up with a way to store data pertaining to the map
#       come up with a interface for the other objects to alter the data on that map
#       figure out how to adjust throttle for slip angle
#####################################

import math # used to find absolute value
class ThrottleController:
    throttlePosition = 0     # the desired position of the throttle
    pendingLaneChange = 0   # change lanes at next switch (-1=left, 0=none, 1=right)

    # called by the bot to get the throttle to send to the server
    def getThrottle(self, slip=0):
        self.adjustForSlip(slip)
        return self.throttlePosition
    
    # called by the bot to see if it should change lanes
    def getLaneChange(self):
        return pendingLaneChange

    # sample of how to adjust for slip, for testing/demo purposes
    def adjustForSlip(self, slip):
        #maxSlip=50
        adjustedSlip=(math.fabs(slip) * 7)/100 # simple inverse function
        if adjustedSlip > 0.85:
            adjustedSlip = 0.85
        self.throttlePosition = 0.85 - adjustedSlip
